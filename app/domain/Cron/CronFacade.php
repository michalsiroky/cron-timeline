<?php declare(strict_types = 1);

namespace App\Domain\Cron;

use App\Model\Database\Entity\Cron;
use App\Model\Database\EntityManager;
use App\Model\Utils\DateTime;
use Cron\CronExpression;
use Doctrine\ORM\Id\AssignedGenerator;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use SplFileObject;

class CronFacade
{

	private EntityManager $entityManager;

	public function __construct(
		EntityManager $entityManager
	) {
		$this->entityManager = $entityManager;
	}

	public function countAllCrones(): int
	{
		return count($this->entityManager->getCronRepository()->findAll());
	}

	/**
	 * @return Cron[]
	 */
	public function findCronesBy(array $criteria, ?array $orderBy = NULL, ?int $limit = NULL, ?int $offset = NULL): array
	{
		return $this->entityManager->getCronRepository()->findBy($criteria, $orderBy, $limit, $offset);
	}

	/**
	 * @return array<int, array<string, int|string>>
	 */
	public function getEventsForCron(DateTime $dateFrom, DateTime $dateTo, ?int $limit = 10, ?int $offset = NULL): array
	{
		$crones = [];
		$events = [];

		$findedCrones = $this->entityManager->getCronRepository()->findBy([], NULL, $limit, $offset);
		foreach ($findedCrones as $cron) {
			$crones[] = [
				'id' => $cron->getId(),
				'name' => $cron->getName(),
			];

			$i = 0;
			$dateNow = $dateFrom;
			$cronExpression = CronExpression::factory($cron->getExpression());
			while($dateNow < $dateTo)
			{
				$dateNow = $cronExpression->getNextRunDate($dateFrom->format('Y-m-d H:i:s'), $i, true);
				$startDate = $dateNow->format('Y-m-d H:i:s');
				$endDate = $dateNow->modify('+' . intval($cron->getAverageTimeProcessing()) . ' seconds')->format('Y-m-d H:i:s');
				$events[] = [
					'name' => $cron->getName() . ' (' .$startDate . ' - ' . $endDate . ')',
					'location' => $cron->getId(),
					'start' => $startDate,
					'end' => $endDate,
					'disabled' => $cron->getStatus() === 'disabled',
				];
				$i++;
			}
		}

		return [$crones, $events];
	}

	public function saveOrUpdateCronesFromCSV(string $pathFile): array
	{
		$file = new SplFileObject($pathFile);
		$file->setFlags(SplFileObject::READ_CSV);

		$metadata = $this->entityManager->getClassMetaData(Cron::class);
		$metadata->setIdGenerator(new AssignedGenerator());
		$metadata->setIdGeneratorType(ClassMetadataInfo::GENERATOR_TYPE_NONE);

		$unsavedRecords = [];
		$batchSize = 20;
		foreach ($file as $i => $item) {
			if ($i === 0 || ! isset($item[0])) {
				continue;
			}

			if (! isset($item[1]) || ! isset($item[2]) || ! isset($item[3]) || ! isset($item[4])
				|| ! isset($item[5]) || ! isset($item[7]) || ! isset($item[8])) {
				$unsavedRecords[$i] = $item[0];
				continue;
			}

			$cron = $this->entityManager->getCronRepository()->find(intval($item[0]));
			if ($cron === NULL) {
				$cron = new Cron();
				$cron->setId(intval($item[0]));
			}

			$cron->setName($item[1]);
			$cron->setExpression($item[2]);
			$cron->setUrl($item[3]);
			$cron->setEmailMe($item[4]);
			$cron->setLog(intval($item[5]));
			$cron->setPost($item[6]);
			$cron->setStatus($item[7]);
			$cron->setAverageTimeProcessing(floatval($item[8]));

			$this->entityManager->persist($cron);

			if (($i % $batchSize) == 0) {
				$this->entityManager->flush();
				$this->entityManager->clear();
			}
		}

		$this->entityManager->flush();
		$this->entityManager->clear();

		return $unsavedRecords;
	}

}
