<?php declare(strict_types = 1);

namespace App\Model\Database\Repository;

use App\Model\Database\Entity\Cron;

/**
 * @method Cron|NULL find($id, ?int $lockMode = NULL, ?int $lockVersion = NULL)
 * @method Cron|NULL findOneBy(array $criteria, array $orderBy = NULL)
 * @method Cron[] findAll()
 * @method Cron[] findBy(array $criteria, array $orderBy = NULL, ?int $limit = NULL, ?int $offset = NULL)
 * @extends AbstractRepository<Cron>
 */
class CronRepository extends AbstractRepository
{

}
