<?php declare(strict_types = 1);

namespace App\Model\Database\Entity;

use App\Model\Database\Entity\Attributes\TId;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Model\Database\Repository\CronRepository")
 * @ORM\Table(name="cron")
 * @ORM\HasLifecycleCallbacks
 */
class Cron extends AbstractEntity
{

	use TId;

	/**
	 * @ORM\Column(type="string", nullable=FALSE)
	 */
	private string $name;

	/**
	 * @ORM\Column(type="string", nullable=FALSE)
	 */
	private string $expression;

	/**
	 * @ORM\Column(type="string", nullable=FALSE)
	 */
	private string $url;

	/**
	 * @ORM\Column(type="string", nullable=FALSE)
	 */
	private string $emailMe;

	/**
	 * @ORM\Column(type="integer", nullable=TRUE)
	 */
	private ?int $log = NULL;

	/**
	 * @ORM\Column(type="string", nullable=TRUE)
	 */
	private ?string $post = NULL;

	/**
	 * @ORM\Column(type="string", nullable=FALSE)
	 */
	private string $status;

	/**
	 * @ORM\Column(type="float", nullable=FALSE)
	 */
	private float $averageTimeProcessing;


	public function getId(): int
	{
		return $this->id;
	}


	public function setId(int $id): void
	{
		$this->id = $id;
	}


	public function getName(): string
	{
		return $this->name;
	}


	public function setName(string $name): void
	{
		$this->name = $name;
	}


	public function getExpression(): string
	{
		return $this->expression;
	}


	public function setExpression(string $expression): void
	{
		$this->expression = $expression;
	}


	public function getUrl(): string
	{
		return $this->url;
	}


	public function setUrl(string $url): void
	{
		$this->url = $url;
	}


	public function getEmailMe(): string
	{
		return $this->emailMe;
	}


	public function setEmailMe(string $emailMe): void
	{
		$this->emailMe = $emailMe;
	}


	public function getLog(): ?int
	{
		return $this->log;
	}


	public function setLog(?int $log): void
	{
		$this->log = $log;
	}


	public function getPost(): ?string
	{
		return $this->post;
	}


	public function setPost(?string $post): void
	{
		$this->post = $post;
	}


	public function getStatus(): string
	{
		return $this->status;
	}


	public function setStatus(string $status): void
	{
		$this->status = $status;
	}


	public function getAverageTimeProcessing(): float
	{
		return $this->averageTimeProcessing;
	}


	public function setAverageTimeProcessing(float $averageTimeProcessing): void
	{
		$this->averageTimeProcessing = $averageTimeProcessing;
	}

}
