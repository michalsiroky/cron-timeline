<?php declare(strict_types = 1);

namespace App\Modules\Front\Home;

use App\Domain\Cron\CronFacade;
use App\Model\Utils\DateTime;
use App\Modules\Front\BaseFrontPresenter;
use Nette\Application\UI\Form;
use Nette\Utils\Paginator;

final class HomePresenter extends BaseFrontPresenter
{

	/** @inject */
	public CronFacade $cronFacade;

	public function renderDefault(int $page = 1, string $forDate = 'midnight'): void
	{
		$dateFrom = new DateTime($forDate);
		$dateTo = (new DateTime($forDate))->modify('+1 day');

		$perPage = 20;
		$countCrones = $this->cronFacade->countAllCrones();
		$paginator = new Paginator;
		$paginator->setItemCount($countCrones);
		$paginator->setItemsPerPage($perPage);
		$paginator->setPage($page);

		[$crones, $events] = $this->cronFacade->getEventsForCron(
			$dateFrom,
			$dateTo,
			$paginator->getLength(),
			$paginator->getOffset()
		);

		$this->template->crones = $crones;
		$this->template->events = $events;
		$this->template->countCrones = $countCrones;
		$this->template->paginator = $paginator;
		$this->template->dateFrom = $dateFrom->format('Y-m-d H:i:s');
		$this->template->dateTo = $dateTo->format('Y-m-d H:i:s');
	}

	protected function createComponentUploadForm(): Form
	{
		$form = new Form();

		$form->addUpload('file', 'Upload CSV');
		// TODO: add validations (mimetype CSV)

		$form->addSubmit('submit', 'IMPORTOVAT');

		$form->onSuccess[] = [$this, 'processUploadForm'];
		return $form;
	}

	public function processUploadForm(Form $form, $values): void
	{
		$unsavedRecords = $this->cronFacade->saveOrUpdateCronesFromCSV($values['file']->getTemporaryFile());

		if(empty($unsavedRecords)) {
			$this->flashSuccess('Všechny crony byly úspěšně uloženy.');
		} else {
			$this->flashError('Během ukládání došlo k problému. Neuložené záznamy: ' . implode(', ', $unsavedRecords));
		}
	}
}
