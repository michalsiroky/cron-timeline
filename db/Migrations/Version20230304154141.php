<?php

declare(strict_types=1);

namespace Database\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230304154141 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE cron (id INT NOT NULL, name VARCHAR(255) NOT NULL, expression VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, email_me VARCHAR(255) NOT NULL, log INT DEFAULT NULL, post VARCHAR(255) DEFAULT NULL, status VARCHAR(255) NOT NULL, average_time_processing DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE cron');
    }
}
